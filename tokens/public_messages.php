<?php
require_once ('const.php');
require_once ('common.php');

$mes = trim($_POST['messages']);
if ($mes == '') exit;

$f = getSesMessFile($_POST['host'], date2intel($_POST['timeBase']), $_POST['winAcc'], $_POST['login'], $_POST['nick']);

if (!file_exists(dirname($f)))
	mkdir (dirname($f), 0777, true );


file_put_contents($f, $mes."\r\n", FILE_APPEND);

?>