<?php
require_once('const.php');
require_once('Session.php');
require_once('Cell.php');

function addMap($var, $inc, $m2){
        $mas = preg_split ( "/;/",$m2 );
        $added = false;
        for( $i = 0; $i < count( $mas ); $i++){
                $eqs = preg_split("/=/",$mas[$i]);
                if($eqs[0] == $var){
                        $mas[$i] = $var . "=" . ($eqs[1] + $inc);
                        $added = true;
                }
        }
        if(!$added){
                array_push($mas, $var . "=" . $inc);
        }
        $m2 = implode( ";", $mas);
        return $m2;
}

function addMaps($m1, $m2) {
		if($m2 == ""){
			$m2 = $m1;
			return $m2;
		}
		if($m1 == ""){
			return $m2;
		}
        $mas = preg_split ( "/;/",$m1 );
        foreach( $mas as $eq ){
                $eqs = preg_split("/=/",$eq);
                $m2 = addMap($eqs[0],$eqs[1],$m2);
        }
		return $m2;
}

function getSelDateTime($selDate) {
	$t = strtotime($selDate->val);
	if ($t - time() > 5*30*24*3600) $t = strtotime($selDate->val.' '.date("Y",strtotime("-1 year")));
	return $t;
}

function cmpSes($s1, $s2) {
	if($s1->pars['host'] == $s2->pars['host'] && isset ( $s1->pars['winAcc'] ) && isset( $s2->pars['winAcc'] ))
		return strcmp($s1->pars['winAcc'], $s2->pars['winAcc']);
	return strcmp($s1->pars['host'], $s2->pars['host']);
}

function readSessions($selDate, $nw) {
	if ($nw == null || $nw == 'all') $nw = '*';
	$st = time();
	$sess = array ();

	$t = getSelDateTime($selDate);
	$sdPrefs = array(date ('Ymd', $t-3600*24*0), date ('Ymd', $t-3600*24*1), date ('Ymd', $t-3600*24*2));

	foreach ($sdPrefs as $sdPref) {
		$pat = DATA_FOLDER."/$nw/$sdPref/*.ses";
		$g = glob($pat);
		#echo "pat=$pat<br>";
		foreach ($g as $sf) {
			#echo "sf=$sf<br>";
			if (strpos(basename($sf), '_') === 0) continue;
			$ses = new Session ( $sf );
			array_push ( $sess, $ses );
		}
	}

	usort($sess, "cmpSes");

	return $sess;
}

function filterSessions($sess, $selNw, $selGtype, $selDate, $selPtc, $selSesStatus, $selHands, $selAlliance) {
	$sess2 = array();

	$selTime = getSelDateTime($selDate);

	foreach ($sess as $ses) {
		$sesTime = strtotime($ses->pars['timeBase']);
		if (date('Ymd', $sesTime) != date ('Ymd', $selTime)) continue;

		if ($selSesStatus->val === 'finished' && $ses->getTimeFinishSched() > time()) continue;
		if ($selSesStatus->val === 'running'
				&& ($ses->getTimeFinishSched() < time() || $ses->getTimeStartSched() > time())) continue;
		if ($selSesStatus->val === 'future' && $ses->getTimeStartSched() < time()) continue;
		if ($selNw->val != 'all' && $ses->pars['network'] != $selNw->val) continue;
		if ($selGtype->val != 'all' && $ses->pars['gType'] != $selGtype->val) continue;
		if ($selPtc->val != 'all' && $ses->pars['ptc'] != $selPtc->val) continue;
		if ($selHands->val != 'all' && $ses->pars['countHand'] >= (int)str_replace('lt', '', $selHands->val)) continue;
		if ($selAlliance->val != 'all' && $selNw->val != 'all' && explode('/', $ses->pars['cid/aid'])[1] != $selAlliance->val) continue;

		if (isset($_GET['ip']) && strpos($ses->pars['host'], $_GET['ip']) === false) continue;

		array_push($sess2, $ses);
	}
	return $sess2;
}

function sesByNick($sess) {
	$nick2sess = array();
	foreach ($sess as $ses) {
		if (!isset($nick2sess[$ses->pars['nick']])) $nick2sess[$ses->pars['nick']] = array();
		array_push($nick2sess[$ses->pars['nick']], $ses);
	}
	return $nick2sess;
}

function getSep($cc) {
	return "<tr style='height:10px;font-size:0;background-color:lightgrey'><td colspan=$cc>&nbsp;</td></tr>";
}

function getRows($sess, $nick2sess, $cells) {
    $tableContent = "";
	$dataHost = "";
    $dataActions = "";

	for ($i = 0; $i < count ( $sess ); $i ++) {
		$ses = $sess [$i];
		$pses = null;
		if ($i > 0) $pses = $sess [$i - 1];
	
		if ($pses == null or $ses->pars['host'] != $pses->pars['host'])
            $tableContent .= getSep ( count ( $cells ) );
	
		$cvals = Cell::getCellVals ( $cells, $sess, $i );
        $tableContent .= Cell::getRow ( $cvals, $sess, $nick2sess, $i );
        $dataHost .= $ses->pars['host'];
        $dataActions .= ($ses->pars['host'] . ':::' . $ses->pars['winAcc'] . ':::' . $ses->pars['login']);

        if ($i + 1 < count($sess)) {
            $dataHost .= ';;;';
            $dataActions .= ';;;';
        }
	}

    $header = "<p><table border=1 cellpadding=6 cellspacing=0>" . Cell::getHeader ( $cells, $dataHost, $dataActions);

	return $header . $tableContent;
}

function get2urlPars() {
	$pars = '';
	foreach ( $_GET as $k => $v )
		$pars .= "&$k=" . urlencode ( $v );
	return $pars;
}

function getUserCmdFile($ip, $winUser, $login) {
	return DATA_FOLDER.'/_cmd/'.$ip.'/'.$winUser.'-'.$login.'.command';
}

function getServerCmdFile($ip, $action) {
	return DATA_FOLDER.'/_cmd/'.$ip.'/'.($action == 'update' ? '/update' : '/command');
}

function getSesErrFile($nw, $ip, $baseTimeIntel, $winAcc, $login, $nick) {
	return DATA_FOLDER."/_err/$nw/$ip/".explode('_', $baseTimeIntel)[0]."/$winAcc-$nick-$login-$baseTimeIntel.err";
}

function getSesMessFile($ip, $baseTimeIntel, $winAcc, $login, $nick) {
    return DATA_FOLDER."/_messages/".explode('_', $baseTimeIntel)[0]."/$ip-$nick-$baseTimeIntel.messages";
}

function getOpt2($map, $key) {
    return array_key_exists($key, $map) ? $map[$key] : null;
}

function date2intel($d) {
	$d = str_replace('/', '', $d);
	$d = str_replace(':', '', $d);
	$d = str_replace(' ', '_', $d);
	return $d;
}

?>