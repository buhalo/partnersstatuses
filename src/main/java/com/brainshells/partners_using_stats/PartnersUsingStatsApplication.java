package com.brainshells.partners_using_stats;


import com.brainshells.partners_using_stats.dao.TgRouterDao;
import com.brainshells.partners_using_stats.service.AdminMessagesService;
import com.brainshells.partners_using_stats.service.GoogleAdminSheetService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.scheduling.annotation.EnableScheduling;
import org.springframework.scheduling.annotation.Scheduled;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.*;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

@EnableScheduling
@SpringBootApplication
public class PartnersUsingStatsApplication implements CommandLineRunner {

    private static boolean oneTimeOnly = true;

    public static void main(String[] args) {
        SpringApplication.run(PartnersUsingStatsApplication.class, args);
    }

    public static enum Month {
        January(0),
        February(1),
        March(2),
        April(3),
        May(4),
        June(5),
        July(6),
        August(7),
        September(8),
        October(9),
        November(10),
        December(11);

        private int value;

        Month(int value) {
            this.value = value;
        }

        public int getValue() {
            return value;
        }

        public static Month getByValue(int i){
            for (Month value : Month.values()) {
                if (value.getValue() == i){
                    return value;
                }
            }
            return null;
        }
    }

    @Autowired
    AdminMessagesService adminMessagesService;
    @Autowired
    TgRouterDao tgRouterDao;
    @Autowired
    GoogleAdminSheetService googleAdminSheetService;

    // key: partnerName in lowerCase without spaces, value: adminFolder (p97),
    private static HashMap<String, String> partnersAdminMap = new HashMap<>();

    private static HashMap<Integer, HashMap<String, String>> messagesForMonthMap = new HashMap<>();

    private static HashMap<String, Integer> handCountsMap = new HashMap<>();
    private static HashMap<String, Integer> countServerActive = new HashMap<>();



    @Override
    public void run(String... args) throws Exception {


        int dayAgo = -1;
        if (args.length > 0) {
            dayAgo = Integer.parseInt(args[0]);
        }

        Map<String, String> adminList = googleAdminSheetService.getAdminList();
        partnersAdminMap.putAll(adminList);


        Map<String, Set<String>> serverListMsk = googleAdminSheetService.getServerListMsk();
        Map<String, Set<String>> serverListOthers = googleAdminSheetService.getServerListOthers();

        LinkedHashSet<String> partnersList = new LinkedHashSet<>(partnersAdminMap.keySet());

        HashMap<String, String> messages = new HashMap<>();
        for (String partnerName : partnersAdminMap.keySet()) {
            String partnerAdminCode = partnersAdminMap.get(partnerName);
            String yesterDayDateAdminFormat = getYesterdayDateSqlFormat(dayAgo).replace("-", "");
            StringBuilder message = new StringBuilder();

            int countNoMoneyMess = adminMessagesService.getNoMoneyMessages(partnerAdminCode, yesterDayDateAdminFormat);
            if (countNoMoneyMess > 0) {
                message.append("No money messages: ");
                message.append(countNoMoneyMess);
                message.append("\n");
            }

            int countNoTablesMess = adminMessagesService.getNoTablesMessages(partnerAdminCode, yesterDayDateAdminFormat);
            if (countNoTablesMess > 0) {
                message.append("No tables messages: ");
                message.append(countNoTablesMess);
                message.append("\n");
            }
            Set<String> allServers = new HashSet<>();

            Set<String> serverSet = serverListMsk.get(partnerName);
            if (serverSet != null) {
                allServers.addAll(serverSet);
            }
            Set<String> serverSetTwo = serverListOthers.get(partnerName);
            if (serverSetTwo != null) {
                allServers.addAll(serverSetTwo);
            }

            if (allServers.size() != 0) {
                Map<String, Integer> banCountMap = tgRouterDao.getBanCount(getYesterdayDateSqlFormat(dayAgo - 1), getYesterdayDateSqlFormat(dayAgo), new ArrayList<>(allServers));
                int banCount = 0;
                for (Integer value : banCountMap.values()) {
                    if (value > 1){
                        banCount++;
                    }
                }

                if (banCount > 0) {
                    message.append("Ban count slow rotation: ");
                    message.append(banCount);
                    message.append("\n");
                }

                int serversCount = tgRouterDao.getActiveServersCount(getYesterdayDateSqlFormat(dayAgo), new ArrayList<>(allServers));

                if (allServers.size() > serversCount) {
                    message.append("Server low usage: ");
                    message.append(serversCount);
                    message.append("/");
                    message.append(allServers.size());
                    message.append("\n");
                }

                int handCount = tgRouterDao.getHandCountForServers(getYesterdayDateSqlFormat(dayAgo), new ArrayList<>(allServers));

                if (serversCount * 2000 > handCount) {
                    message.append("Low hands played: ");
                    message.append(handCount);
                    message.append("/");
                    message.append(serversCount);
                    message.append(" servers");
                    message.append("\n");
                }

            }
            messages.put(partnerName, message.toString());
        }

        Calendar yesterdayCalendar = getYesterdayCalendar(dayAgo);
        int yesterday = yesterdayCalendar.get(Calendar.DAY_OF_MONTH);
        Calendar today = getYesterdayCalendar(0);
        int todayDay = today.get(Calendar.DAY_OF_MONTH);
        String month = Month.getByValue(yesterdayCalendar.get(Calendar.MONTH)).name();
        if (!oneTimeOnly) {
            googleAdminSheetService.eraseSheet(month);
        }
            googleAdminSheetService.writePartnersList(month, partnersList);
        if (!oneTimeOnly) {
            for (int i = 1; i < yesterday; i++) {
                HashMap<String, String> msgs = messagesForMonthMap.getOrDefault(i, new HashMap<>());
                if (msgs.isEmpty()) {
                    continue;
                }
                googleAdminSheetService.writeToFinalSheet(msgs, month, i, partnersList);
                Thread.sleep(5000);
            }
        }
        googleAdminSheetService.writeToFinalSheet(messages, month, yesterday,partnersList);

        messagesForMonthMap.put(yesterday, messages);
        if (todayDay == 1){
            messagesForMonthMap.clear();
        }
    }


    private String getYesterdayDateSqlFormat(int dayAgo) {
        DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
        return dateFormat.format(yesterday(dayAgo));
    }

    private Date yesterday(int dayAgo) {
        final Calendar cal = Calendar.getInstance();
        cal.add(Calendar.DATE, dayAgo);
        return cal.getTime();
    }

    private Calendar getYesterdayCalendar(int dayAgo) {
        final Calendar cal = Calendar.getInstance();
        cal.add(Calendar.DATE, dayAgo);
        return cal;
    }

    @Scheduled(cron = "0 0 6 * * ?")
    public void startStatusMaker(){
        try {
run();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Scheduled(initialDelay = 1000 * 30, fixedDelay=Long.MAX_VALUE)
    public void initBeforeFiller() {
        try {
            for (int i = -48; i < 0; i++) {
                run(String.valueOf(i));
                Thread.sleep(5000);
            }
            oneTimeOnly = false;
        } catch (Exception e){
            e.printStackTrace();
        }
    }



}
