package com.brainshells.partners_using_stats.service;

import org.springframework.stereotype.Service;

import java.io.*;

@Service
public class AdminMessagesService {
    private String pattern = "\\\\212.22.77.66\\c$\\admin\\shells_%s\\admin\\data2\\_messages";
    private final String noMoney = "no_money";
    private final String noTables = "no_tables";

    public int getNoMoneyMessages(String partnerNum, String date) throws IOException {
        return countMessages(partnerNum, date, noMoney);
    }

    public int getNoTablesMessages(String partnerNum, String date) throws IOException {
        return countMessages(partnerNum, date, noTables);
    }

    private int countMessages(String partnerNum, String date, String message) throws IOException {
        int countNoMoneyMessages = 0;
        String formattedAddress = String.format(pattern, partnerNum);
        File formattedFolder = new File(formattedAddress);
        if (formattedFolder.listFiles() == null){
            return countNoMoneyMessages;
        }
        for (File folder : formattedFolder.listFiles()) {
            if (folder != null && folder.getName().endsWith(date)){
                if (folder.listFiles() != null) {
                    for (File file : folder.listFiles()) {
                        BufferedReader reader = new BufferedReader(new FileReader(file));
                        String line = null;
                        while ((line = reader.readLine()) != null) {
                            if (line.contains(message)){
                                countNoMoneyMessages++;
                            }
                        }
                    }
                }

            }
        }
        return countNoMoneyMessages;
    }

}
