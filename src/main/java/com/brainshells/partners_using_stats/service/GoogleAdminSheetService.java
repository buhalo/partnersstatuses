package com.brainshells.partners_using_stats.service;

import com.brainshells.partners_using_stats.PartnersUsingStatsApplication;
import com.google.api.client.auth.oauth2.Credential;
import com.google.api.client.extensions.java6.auth.oauth2.AuthorizationCodeInstalledApp;
import com.google.api.client.extensions.jetty.auth.oauth2.LocalServerReceiver;
import com.google.api.client.googleapis.auth.oauth2.GoogleAuthorizationCodeFlow;
import com.google.api.client.googleapis.auth.oauth2.GoogleClientSecrets;
import com.google.api.client.googleapis.javanet.GoogleNetHttpTransport;
import com.google.api.client.http.javanet.NetHttpTransport;
import com.google.api.client.json.JsonFactory;
import com.google.api.client.json.gson.GsonFactory;
import com.google.api.client.util.store.FileDataStoreFactory;
import com.google.api.services.sheets.v4.Sheets;
import com.google.api.services.sheets.v4.SheetsScopes;
import com.google.api.services.sheets.v4.model.*;
import org.springframework.stereotype.Service;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.security.GeneralSecurityException;
import java.util.*;

@Service
public class GoogleAdminSheetService {

    private static final String APPLICATION_NAME = "Google Sheets API Java Quickstart";
    private static final JsonFactory JSON_FACTORY = GsonFactory.getDefaultInstance();
    private static final String TOKENS_DIRECTORY_PATH = "tokens";
    private static final List<String> SCOPES = Collections.singletonList(SheetsScopes.SPREADSHEETS_READONLY);
    private static final List<String> SCOPES_WRITE = Collections.singletonList(SheetsScopes.SPREADSHEETS);
    private static final String CREDENTIALS_FILE_PATH = "/credentials.json";


    private Credential getCredentials(final NetHttpTransport HTTP_TRANSPORT) throws IOException {
        // Load client secrets.
        InputStream in = PartnersUsingStatsApplication.class.getResourceAsStream(CREDENTIALS_FILE_PATH);
        if (in == null) {
            throw new FileNotFoundException("Resource not found: " + CREDENTIALS_FILE_PATH);
        }
        GoogleClientSecrets clientSecrets = GoogleClientSecrets.load(JSON_FACTORY, new InputStreamReader(in));

        // Build flow and trigger user authorization request.
        GoogleAuthorizationCodeFlow flow = new GoogleAuthorizationCodeFlow.Builder(
                HTTP_TRANSPORT, JSON_FACTORY, clientSecrets, SCOPES_WRITE)
                .setDataStoreFactory(new FileDataStoreFactory(new java.io.File(TOKENS_DIRECTORY_PATH)))
                .setAccessType("offline")
                .build();
        LocalServerReceiver receiver = new LocalServerReceiver.Builder().setPort(8888).build();
        return new AuthorizationCodeInstalledApp(flow, receiver).authorize("user");
    }


    public Map<String, String> getAdminList() throws IOException, GeneralSecurityException {
        HashMap<String, String> adminMap = new HashMap<>();
        final NetHttpTransport HTTP_TRANSPORT = GoogleNetHttpTransport.newTrustedTransport();
        final String spreadsheetId = "1wlC97BkJS07ppcnxLUQRu0C2tshVWdMLJaDb8AAPzSY";
        final String range = "Админки!A2:E";
        Sheets service = new Sheets.Builder(HTTP_TRANSPORT, JSON_FACTORY, getCredentials(HTTP_TRANSPORT))
                .setApplicationName(APPLICATION_NAME)
                .build();
        ValueRange response = service.spreadsheets().values()
                .get(spreadsheetId, range)
                .execute();
        List<List<Object>> values = response.getValues();
        if (values == null || values.isEmpty()) {
            System.out.println("No data found.");
        } else {
            for (List row : values) {
                if (row.size() > 2) {
                    String logPass = (String) row.get(1);
                    String partnerName = (String) row.get(2);
                    if (logPass != null && partnerName != null) {
                        String[] split = logPass.split("/");
                        if (split.length >= 2) {
                            if (partnerName.contains("RP") || partnerName.contains("YP")) {
                                adminMap.put(partnerName, split[0]);
                            }
                        }
                    }
                }
            }
        }
        return adminMap;
    }

    public Map<String, Set<String>> getServerListMsk() throws IOException, GeneralSecurityException {
        HashMap<String, Set<String>> adminMap = new HashMap<>();
        final NetHttpTransport HTTP_TRANSPORT = GoogleNetHttpTransport.newTrustedTransport();
        final String spreadsheetId = "1zrMc3W_YoyYsbIher96R4KyY6d0OR7YbXmohe7uQGtM";
        final String range = "Сервера МСК!B21:D";
        Sheets service = new Sheets.Builder(HTTP_TRANSPORT, JSON_FACTORY, getCredentials(HTTP_TRANSPORT))
                .setApplicationName(APPLICATION_NAME)
                .build();
        ValueRange response = service.spreadsheets().values().get(spreadsheetId, range).execute();
        List<List<Object>> values = response.getValues();
        if (values == null || values.isEmpty()) {
            System.out.println("No data found.");
        } else {
            for (List row : values) {
                fillAdminMap(adminMap, row);
            }
        }
        return adminMap;
    }

    public Map<String, Set<String>> getServerListOthers() throws IOException, GeneralSecurityException {
        HashMap<String, Set<String>> adminMap = new HashMap<>();
        final NetHttpTransport HTTP_TRANSPORT = GoogleNetHttpTransport.newTrustedTransport();
        final String spreadsheetId = "1KU1SVYgi6Q9nr8o7vRFFsA8k9azHcHL4fE2MLPLS9Rw";
        final String range = "!!! Все сервера!B20:D";
        Sheets service = new Sheets.Builder(HTTP_TRANSPORT, JSON_FACTORY, getCredentials(HTTP_TRANSPORT))
                .setApplicationName(APPLICATION_NAME)
                .build();
        ValueRange response = service.spreadsheets().values().get(spreadsheetId, range).execute();
        List<List<Object>> values = response.getValues();
        if (values == null || values.isEmpty()) {
            System.out.println("No data found.");
        } else {
            for (List row : values) {
                fillAdminMap(adminMap, row);
            }
        }
        return adminMap;
    }

    private void fillAdminMap(HashMap<String, Set<String>> adminMap, List row) {
        if (row.size() < 3){
            return;
        }
        String partner = (String) row.get(2);
        String server =(String) row.get(0);
        Set<String> serverList = adminMap.getOrDefault(partner, new HashSet<>());
        serverList.add(server);
        adminMap.put(partner,serverList);
    }


    public LinkedHashSet<String> getPartnerListFromFinalSheet() throws IOException, GeneralSecurityException{
        LinkedHashSet <String> partnersSet = new LinkedHashSet <>();
        final NetHttpTransport HTTP_TRANSPORT = GoogleNetHttpTransport.newTrustedTransport();
        final String spreadsheetId = "1TnGEMYT_wvNhdbU9RL_OMnktDsCrHCFTb-7fXDVsGto";
        final String range = "April!A2:A";
        Sheets service = new Sheets.Builder(HTTP_TRANSPORT, JSON_FACTORY, getCredentials(HTTP_TRANSPORT))
                .setApplicationName(APPLICATION_NAME)
                .build();
        ValueRange response = service.spreadsheets().values().get(spreadsheetId, range).execute();
        List<List<Object>> values = response.getValues();
        if (values == null || values.isEmpty()) {
            System.out.println("No data found.");
        } else {
            for (List row : values) {
                if (row.size() < 1){
                    continue;
                }
                String partnerRaw = (String) row.get(0);
                String[] split = partnerRaw.split("\\(");
                String partner = split[0].replaceAll("\\s+", "").toLowerCase();
                partnersSet.add(partner);
            }
        }
        return partnersSet;
    }

    public void writeToFinalSheet(HashMap<String,String> message, String month, int day, LinkedHashSet<String> partners) throws Exception {
        final NetHttpTransport HTTP_TRANSPORT = GoogleNetHttpTransport.newTrustedTransport();
        final String spreadsheetId = "1QhiUSPrN96ujMIm7fpBVjNBLjqqbEnw1-TOfblah0KY";
        int index = 2;
        List<String> list = Arrays.asList("a","b","c","d","e","f","g","h","i","j","k","l","m","n","o","p","q","r","s","t","u","v","w","x","y","z","aa", "ab", "ac", "ad", "ae", "af", "ag", "ax");
        String dayChar = list.get(day);
        List<List<Object>> valuesToWrite = new ArrayList<>();
        StringBuilder range = new StringBuilder(month);
        range.append("!");
        range.append(dayChar);
        range.append(2);
        range.append(":");
        range.append("AZ999");
        for (String partner : partners) {
            index++;
            List<Object> values = new ArrayList<>();
            values.add(message.get(partner));
            valuesToWrite.add(values);
        }

        ValueRange body = new ValueRange()
                .setValues(valuesToWrite);
        getService(HTTP_TRANSPORT).spreadsheets().values()
                .update(spreadsheetId, range.toString(), body)
                .setValueInputOption("RAW")
                .execute();

    }

    private Sheets getService(NetHttpTransport HTTP_TRANSPORT) throws Exception{
        Sheets s = new Sheets.Builder(HTTP_TRANSPORT, JSON_FACTORY, getCredentials(HTTP_TRANSPORT))
                .setApplicationName(APPLICATION_NAME)
                .build();
        return s;
    }

    public void writePartnersList(String month, LinkedHashSet<String> partnersList) throws Exception {
        final NetHttpTransport HTTP_TRANSPORT = GoogleNetHttpTransport.newTrustedTransport();
        final String spreadsheetId = "1QhiUSPrN96ujMIm7fpBVjNBLjqqbEnw1-TOfblah0KY";
        int index = 2;
        List<List<Object>> valuesToWrite = new ArrayList<>();
        StringBuilder range = new StringBuilder(month);
        range.append("!");
        range.append("a");
        range.append(2);
        range.append(":");
        range.append("A999");

        for (String partner : partnersList) {
            index++;
            List<Object> values = new ArrayList<>();
            values.add(partner);
            valuesToWrite.add(values);
        }

        ValueRange body = new ValueRange()
                .setValues(valuesToWrite);
        getService(HTTP_TRANSPORT).spreadsheets().values()
                .update(spreadsheetId, range.toString(), body)
                .setValueInputOption("RAW")
                .execute();
    }

    public void eraseSheet(String month) {
        try {
            final NetHttpTransport HTTP_TRANSPORT = GoogleNetHttpTransport.newTrustedTransport();
            String range = month+"!A"+(2)+":"+"AZ999";
            ClearValuesRequest requestBody = new ClearValuesRequest();
            Sheets.Spreadsheets.Values.Clear request =
                    getService(HTTP_TRANSPORT).spreadsheets().values().clear("1QhiUSPrN96ujMIm7fpBVjNBLjqqbEnw1-TOfblah0KY", range, requestBody);
            request.execute();
        }catch (Exception e){
            e.printStackTrace();
        }
    }
}
