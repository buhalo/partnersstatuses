package com.brainshells.partners_using_stats.dao;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.support.rowset.SqlRowSet;
import org.springframework.stereotype.Repository;

import java.util.*;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

@Repository
public class TgRouterDao {

    @Autowired
    JdbcTemplate jdbcTemplate;

    private String getIp (String ipString){
        String IPADDRESS_PATTERN =
                "(?:(?:25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)\\.){3}(?:25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)";

        Pattern pattern = Pattern.compile(IPADDRESS_PATTERN);
        Matcher matcher = pattern.matcher(ipString);
        if (matcher.find()) {
            return matcher.group();
        } else{
            return "0.0.0.0";
        }
    }


    public int getHandCountForServers(String date, List<String> hosts){
        String sql = "select sum(counthand) from sessions where (";
        StringBuilder in = new StringBuilder();
        int hostIndex = 0;
        for (String host : hosts) {
            in.append("host ");
            in.append("like ");
            in.append("'%");
            in.append(host);
            in.append("%'");
            if (hostIndex < hosts.size()-1)
            in.append(" OR ");
            hostIndex++;
        }
       /* if( in.length() > 1 ) {
            in.deleteCharAt(in.length() -6);
        }*/
        sql += in.toString() + ") and day = '" + date + "'";
        SqlRowSet sqlRowSet = jdbcTemplate.queryForRowSet(sql);
        sqlRowSet.next();
        return sqlRowSet.getInt("sum");
    }

    public int getActiveServersCount(String date, List<String> hosts){
        String sql = "select host from sessions where (";
        StringBuilder in = new StringBuilder();
        int hostIndex = 0;
        for (String host : hosts) {
            in.append("host ");
            in.append("like ");
            in.append("'%");
            in.append(host);
            in.append("%'");
            if (hostIndex < hosts.size()-1)
                in.append(" OR ");
            hostIndex++;
        }
        sql += in.toString() + ") and day = '" + date + "'";
        SqlRowSet sqlRowSet = jdbcTemplate.queryForRowSet(sql);
        Set<String> serverSet = new HashSet<>();
        while (sqlRowSet.next()){
            serverSet.add(getIp( sqlRowSet.getString("host")));
        }
        return serverSet.size();
    }

    public Map<String, Integer> getBanCount(String dateBefore, String dateAfter, List<String> hosts){
        Map<String, Integer> pidBanCount = new HashMap<>();
        String sql = "select distinct nick, count(banned) AS bann from sessions where (";
        StringBuilder in = new StringBuilder();
        int hostIndex = 0;
        for (String host : hosts) {
            in.append("host ");
            in.append("like ");
            in.append("'%");
            in.append(host);
            in.append("%'");
            if (hostIndex < hosts.size()-1)
                in.append(" OR ");
            hostIndex++;
        }
        sql += in.toString() + ") and day between '" + dateBefore + "' and '" + dateAfter + "' and banned = true GROUP BY nick";
        SqlRowSet sqlRowSet = jdbcTemplate.queryForRowSet(sql);
        while (sqlRowSet.next()){
            pidBanCount.put(sqlRowSet.getString("nick"), sqlRowSet.getInt("bann"));
        }
        return pidBanCount;
    }


}
